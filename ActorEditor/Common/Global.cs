﻿using ActorEditor.Model;
using ActorEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil.Log;

namespace ActorEditor.Common
{
    class Global
    {
        public static Setting setting;
        public static ObservableCollection<ActorDefinitionViewModel> actorCollection           { get; set; } = new ObservableCollection<ActorDefinitionViewModel>();
        public static ObservableCollection<PhysicsInfoViewModel> physicsInfoCollection         { get; set; } = new ObservableCollection<PhysicsInfoViewModel>();
        public static ObservableCollection<ActorTypeViewModel> actorTypeCollection             { get; set; } = new ObservableCollection<ActorTypeViewModel>();
        public static ObservableCollection<ModelDefinitionViewModel> modelDefinitionCollection { get; set; } = new ObservableCollection<ModelDefinitionViewModel>();
        public static ObservableCollection<string> modelList { get; set; } = new ObservableCollection<string>();
        public static ObservableCollection<string> animList { get; set; } = new ObservableCollection<string>();
        public static LogViewModel Log { get; set; } = new LogViewModel();
    }
}
