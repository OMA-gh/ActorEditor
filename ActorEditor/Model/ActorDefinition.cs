﻿using ActorEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorEditor.Model
{
    class ActorDefinition : WPFUtil.ViewModelBase
    {
        public string _name;
        public string _type;
        public string _modelKey;
        public string _physicsType;
    }
}
