﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ActorEditor.Model
{
    enum PhysicsType
    {
        None=0,
        Dynamic,
        Static,
        CharacterController
    }
    enum PhysicsShape
    {
        None = 0,
        Box,
        Sphere,
        Capsule
    }

    class PhysicsInfo
    {
        public string _name;
        public PhysicsType _type = PhysicsType.None;
        public PhysicsShape _shape = PhysicsShape.None;
        public float _centerPosX;
        public float _centerPosY;
        public float _centerPosZ;
        public float _scaleX;
        public float _scaleY;
        public float _scaleZ;
    }
}
