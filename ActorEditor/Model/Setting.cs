﻿using ActorEditor.Common;
using ActorEditor.Util;
using ActorEditor.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.Model
{
    class Setting : WPFUtil.ViewModelBase
    {
        private string _settingPath;
        private string _rawDefinitionPath;
        private string _rawContentPath;
        private string _rawActorTypeCodeGenDir;

        public string DefinitionPath { get; set; }
        public string ContentPath { get; set; }
        public string ActorTypeCodeGenDir { get; set; }

        public WPFUtil.DelegateCommand OutputSetting { get; private set; }

        public string SettingPath
        {
            get
            {
                return _settingPath;
            }
            set
            {
                _settingPath = value;
                RaisePropertyChanged("SettingPath");
            }
        }
        public string RawDefinitionPath
        {
            get
            {
                return _rawDefinitionPath;
            }
            set
            {
                _rawDefinitionPath = value;
                RaisePropertyChanged("RawDefinitionPath");
            }
        }
        public string RawContentPath
        {
            get
            {
                return _rawContentPath;
            }
            set
            {
                _rawContentPath = value;
                RaisePropertyChanged("RawContentPath");
            }
        }
        public string RawActorTypeCodeGenDir
        {
            get
            {
                return _rawActorTypeCodeGenDir;
            }
            set
            {
                _rawActorTypeCodeGenDir = value;
                RaisePropertyChanged("RawActorTypeCodeGenDir");
            }
        }

        public Setting()
        {
            OutputSetting = new WPFUtil.DelegateCommand(outputSetting);
        }

        public void loadSetting()
        {
            try
            {
                var args = Environment.GetCommandLineArgs();
                int index = 0;
                foreach (var arg in args)
                {
                    if (arg.Equals("--setting"))
                    {
                        SettingPath = Environment.ExpandEnvironmentVariables(args[index + 1]);
                    }
                    index++;
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
                SettingPath = @"D:\home\Project\TestLib\GLtest\Setting\";
            }
            try
            {
                string load_path = SettingPath + @"/Setting.yml";
                using (var input = new StreamReader(load_path, Encoding.UTF8))
                {
                    var yaml = new YamlStream();
                    yaml.Load(input);
                    var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;
                    {
                        RawDefinitionPath = mapping.Children["DefinitionPath"].ToString();
                        DefinitionPath = Environment.ExpandEnvironmentVariables(RawDefinitionPath);
                    }
                    {
                        RawContentPath = mapping.Children["ContentPath"].ToString();
                        ContentPath = Environment.ExpandEnvironmentVariables(RawContentPath);
                    }
                    if (YamlUtil.Contains(mapping, "ActorTypeCodeGenDir"))
                    {
                        RawActorTypeCodeGenDir = mapping.Children["ActorTypeCodeGenDir"].ToString();
                        ActorTypeCodeGenDir = Environment.ExpandEnvironmentVariables(RawActorTypeCodeGenDir);
                    }
                    else
                    {
                        WarningBox.showMessage("ActorTypeCodeGenDirが設定ファイルにありません");
                    }
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }

        public void outputSetting()
        {
            Global.Log.ShowWindow();
            Task.Run(() =>
            {
                using (StreamWriter writer = new StreamWriter(SettingPath + "/Setting.yml", false, Encoding.UTF8))
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine($"DefinitionPath: '{RawDefinitionPath}'");
                    builder.AppendLine($"ContentPath: '{RawContentPath}'");
                    builder.AppendLine($"ActorTypeCodeGenDir: '{RawActorTypeCodeGenDir}'");
                    writer.Write(builder.ToString());
                    Global.Log.WriteLine($"Write Definition Path: {RawDefinitionPath}");
                    Global.Log.WriteLine($"Write Content Path: {RawContentPath}");
                    Global.Log.WriteLine($"Write ActorTypeCodeGenDir: {RawActorTypeCodeGenDir}");
                }
                Global.Log.CloseWindow();
            });
        }
    }
}
