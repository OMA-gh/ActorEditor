﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorEditor.Model
{
    class ModelDefinition
    {
        public string _name;
        public string _modelPath;
        public string _animationPath;
    }
}
