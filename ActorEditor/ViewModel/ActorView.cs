﻿using ActorEditor.Common;
using ActorEditor.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using WPFUtil.Log;

namespace ActorEditor.ViewModel
{
    class ActorView : ViewModelBase
    {
        private ActorDefinitionViewModel _selectData = null;

        public ObservableCollection<ActorDefinitionViewModel> ActorCollection
        {
            get
            {
                return Global.actorCollection;
            }
            set
            {
                Global.actorCollection = value;
                RaisePropertyChanged("ActorCollection");
            }
        }
        public ActorDefinitionViewModel SelectData
        {
            get
            {
                return _selectData;
            }
            set
            {
                _selectData = value;
                RaisePropertyChanged("SelectData");
            }
        }
        public Setting Setting
        {
            get
            {
                return Global.setting;
            }
            set
            {
                Global.setting = value;
                RaisePropertyChanged("Setting");
            }
        }

        public DelegateCommand WriteDefinitionCommand { get; private set; }
        public DelegateCommand AddDefinitionCommand { get; private set; }

        public ActorView()
        {
            WriteDefinitionCommand = new DelegateCommand(WriteActorDefinition);
            AddDefinitionCommand   = new DelegateCommand(AddActorDefinition);
        }

        /*
         *  アクター定義を読み込む
         */
        public void LoadActorDefinition()
        {
            ActorCollection = new ObservableCollection<ActorDefinitionViewModel>();
            try
            {
                if (!Directory.Exists(Setting.DefinitionPath))
                {
                    WarningBox.showMessage($"定義ディレクトリが存在しませんでした。\n[{Setting.DefinitionPath}]");
                    return;
                }
                string dir_path = Setting.DefinitionPath + "/Actor";
                string[] files = Directory.GetFiles(dir_path, "*.yml");

                foreach (var file in files)
                {
                    try
                    {
                        var actor_def = new ActorDefinitionViewModel();
                        if (actor_def.Load(file))
                        {
                            ActorCollection.Add(actor_def);
                        }
                    }
                    catch (Exception e)
                    {
                        WarningBox.showWarning(e, "load file:" + file);
                    }
                }
            }
            catch (Exception ex)
            {
                WarningBox.showWarning(ex);
            }
        }

        /*
         *  アクター定義を書き出す
         */
        public void WriteActorDefinition()
        {
            Global.Log.ShowWindow();
            Task.Run(() =>
            {
                try
                {
                    Global.Log.WriteLine("---- Start Write Actors -----------------------");
                    foreach (var actor in ActorCollection)
                    {
                        string write_dir = Setting.DefinitionPath + "/Actor/";
                        using (StreamWriter writer = new StreamWriter(write_dir + actor.Name + ".yml", false, Encoding.UTF8))
                        {
                            StringBuilder builder = new StringBuilder();
                            ActorDefinitionViewModel.Write(ref builder, actor, "");
                            writer.Write(builder.ToString());
                            Global.Log.WriteLine($"write {actor.Name}");
                        }
                    }
                    Global.Log.WriteLine("Finish Write Actor!");
                    Global.Log.WriteLine("Write Actor List");
                    string content_dir = Setting.ContentPath + "/Actor/";
                    if (!Directory.Exists(content_dir))
                    {
                        Directory.CreateDirectory(content_dir);
                    }
                    Global.Log.WriteLine($"out : {content_dir + "ActorList.yml"}");
                    using (StreamWriter writer = new StreamWriter(content_dir + "ActorList.yml", false, Encoding.UTF8))
                    {
                        StringBuilder builder = new StringBuilder();
                        foreach (var actor in ActorCollection)
                        {
                            builder.AppendLine($"{actor.Name}:");
                            ActorDefinitionViewModel.Write(ref builder, actor, "  ");
                        }
                        writer.Write(builder.ToString());
                    }
                    Global.Log.WriteLine("Finish Write ActorList!");
                }
                catch (Exception e)
                {
                    WarningBox.showWarning(e);
                }
                finally
                {
                    Global.Log.CloseWindow();
                }
            });
        }

        /*
         *  アクター定義を追加する
         */
        public void AddActorDefinition()
        {
            try
            {
                var actor_def = new ActorDefinitionViewModel();
                ActorCollection.Add(actor_def);
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }
    }
}
