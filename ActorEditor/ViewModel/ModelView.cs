﻿using ActorEditor.Common;
using ActorEditor.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;

namespace ActorEditor.ViewModel
{
    class ModelView : ViewModelBase
    {
        private ModelDefinitionViewModel _selectData = null;

        public ModelDefinitionViewModel SelectData
        {
            get
            {
                return _selectData;
            }
            set
            {
                _selectData = value;
                RaisePropertyChanged("SelectData");
            }
        }
        public Setting Setting
        {
            get
            {
                return Global.setting;
            }
            set
            {
                Global.setting = value;
                RaisePropertyChanged("Setting");
            }
        }
        public ObservableCollection<ModelDefinitionViewModel> ModelDefinitionCollection
        {
            get
            {
                return Global.modelDefinitionCollection;
            }
            set
            {
                Global.modelDefinitionCollection = value;
                RaisePropertyChanged("ModelDefinitionCollection");
            }
        }

        public DelegateCommand WriteDefinitionCommand { get; private set; }
        public DelegateCommand AddDefinitionCommand { get; private set; }

        public ModelView()
        {
            WriteDefinitionCommand = new DelegateCommand(WriteModelDefinition);
            AddDefinitionCommand = new DelegateCommand(AddModelDefinition);
        }

        /*
         *  モデル定義を書き出す
         */
        public void WriteModelDefinition()
        {
            Global.Log.ShowWindow();
            foreach (var definition in ModelDefinitionCollection)
            {
                if (!definition.IsValid)
                {
                    continue;
                }
                string write_dir = Setting.DefinitionPath + "/ModelDefinition/";
                if (!Directory.Exists(write_dir))
                {
                    Directory.CreateDirectory(write_dir);
                }
                Global.Log.WriteLine("---- Write Model Definition -----------------");
                using (StreamWriter writer = new StreamWriter(write_dir + definition.Name + ".yml", false, Encoding.UTF8))
                {
                    StringBuilder builder = new StringBuilder();
                    ModelDefinitionViewModel.WriteYaml(ref builder, definition, "");
                    writer.Write(builder.ToString());
                    Global.Log.WriteLine($"write {definition.Name}");
                }
            }
            Global.Log.WriteLine("Finish Write Model Definition!");
            Global.Log.WriteLine("Write Model Definition List");
            string content_dir = Setting.ContentPath + "/ModelDefinition/";
            if (!Directory.Exists(content_dir))
            {
                Directory.CreateDirectory(content_dir);
            }
            Global.Log.WriteLine($"out : {content_dir + "ModelDefinitionList.yml"}");
            using (StreamWriter writer = new StreamWriter(content_dir + "ModelDefinitionList.yml", false, Encoding.UTF8))
            {
                StringBuilder builder = new StringBuilder();
                foreach (var definition in ModelDefinitionCollection)
                {
                    if (!definition.IsValid)
                    {
                        continue;
                    }
                    builder.AppendLine($"{definition.Name}:");
                    ModelDefinitionViewModel.WriteYaml(ref builder, definition, "  ");
                }
                writer.Write(builder.ToString());
            }
            Global.Log.WriteLine("Finish Write ActorList!");
            Global.Log.CloseWindow();
        }

        /*
         *  モデル定義を読み込む
         */
        public void LoadModelDefinition()
        {
            try
            {
                var dir_path = Setting.DefinitionPath + "/ModelDefinition";
                if (!Directory.Exists(dir_path))
                {
                    return;
                }
                var files = Directory.GetFiles(dir_path);
                foreach (var file in files)
                {
                    var add_definition = new ModelDefinitionViewModel();
                    add_definition.LoadYaml(file);
                    ModelDefinitionCollection.Add(add_definition);
                }
                {
                    var add_definition = new ModelDefinitionViewModel();
                    add_definition.Name = "none";
                    add_definition.IsValid = false;
                    ModelDefinitionCollection.Add(add_definition);
                }
                {
                    var add_definition = new ModelDefinitionViewModel();
                    add_definition.Name = "plane";
                    add_definition.IsValid = false;
                    ModelDefinitionCollection.Add(add_definition);
                }
                {
                    var add_definition = new ModelDefinitionViewModel();
                    add_definition.Name = "cube";
                    add_definition.IsValid = false;
                    ModelDefinitionCollection.Add(add_definition);
                }
                {
                    var add_definition = new ModelDefinitionViewModel();
                    add_definition.Name = "torus";
                    add_definition.IsValid = false;
                    ModelDefinitionCollection.Add(add_definition);
                }
                {
                    var add_definition = new ModelDefinitionViewModel();
                    add_definition.Name = "terrain";
                    add_definition.IsValid = false;
                    ModelDefinitionCollection.Add(add_definition);
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }

        /*
         *  モデル定義を追加する
         */
        public void AddModelDefinition()
        {
            try
            {
                var add_definition = new ModelDefinitionViewModel();
                ModelDefinitionCollection.Add(add_definition);
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }

        /*
         *  使用するモデルのリストを作成する
         */
        public void GatherModelList()
        {
            try
            {
                string model_content_dir = Global.setting.ContentPath + "/Model";
                if (!Directory.Exists(model_content_dir))
                {
                    return;
                }
                var files = Directory.GetFiles(model_content_dir);
                foreach (var file in files)
                {
                    Global.modelList.Add("Model/" + Path.GetFileName(file));
                }
            }
            catch(Exception e){
                WarningBox.showWarning(e);
            }
            try
            {
                string anim_content_dir = Global.setting.ContentPath + "/Animation";
                if (!Directory.Exists(anim_content_dir))
                {
                    return;
                }
                var files = Directory.GetFiles(anim_content_dir);
                foreach (var file in files)
                {
                    Global.animList.Add("Animation/" + Path.GetFileName(file));
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }
    }
}
