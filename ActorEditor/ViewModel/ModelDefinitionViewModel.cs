﻿using ActorEditor.Common;
using ActorEditor.Model;
using ActorEditor.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.ViewModel
{
    class ModelDefinitionViewModel : ViewModelBase
    {
        ModelDefinition definition = new ModelDefinition();
        public bool IsValid { get; set; } = true;

        public string Name
        {
            get
            {
                return definition._name;
            }
            set
            {
                definition._name = value;
                RaisePropertyChanged("Name");
            }
        }
        public string ModelPath
        {
            get
            {
                return definition._modelPath;
            }
            set
            {
                definition._modelPath = value;
                RaisePropertyChanged("ModelPath");
            }
        }
        public string AnimationPath
        {
            get
            {
                return definition._animationPath;
            }
            set
            {
                definition._animationPath = value;
                RaisePropertyChanged("AnimationPath");
            }
        }
        public ObservableCollection<string> ModelList
        {
            get
            {
                return Global.modelList;
            }
            private set { }
        }
        public ObservableCollection<string> AnimList
        {
            get
            {
                return Global.animList;
            }
            private set { }
        }

        public static void WriteYaml(ref StringBuilder builder, ModelDefinitionViewModel definition, string offset)
        {
            builder.AppendLine($"{offset}ModelPath: {definition.ModelPath}");
            builder.AppendLine($"{offset}AnimationPath: {definition.AnimationPath}");
        }
        public bool LoadYaml(string path)
        {
            try
            {
                using (var input = new StreamReader(path, Encoding.UTF8))
                {
                    var yaml = new YamlStream();
                    yaml.Load(input);
                    var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;

                    Name = Path.GetFileNameWithoutExtension(path);
                    if (YamlUtil.Contains(mapping, "ModelPath"))
                    {
                        this.ModelPath= mapping.Children["ModelPath"].ToString();
                    }
                    else
                    {
                        WarningBox.showMessage($"{path}の読み込みに失敗");
                        return false;
                    }
                    if (YamlUtil.Contains(mapping, "AnimationPath"))
                    {
                        this.AnimationPath = mapping.Children["AnimationPath"].ToString();
                    }
                    else
                    {
                        WarningBox.showMessage($"{path}の読み込みに失敗");
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
                return false;
            }
            return true;
        }
    }
}
