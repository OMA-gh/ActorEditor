﻿using ActorEditor.Common;
using ActorEditor.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;

namespace ActorEditor.ViewModel
{
    class ActorTypeView : ViewModelBase
    {
        private ActorTypeViewModel _selectData = null;

        public ObservableCollection<ActorTypeViewModel> ActorTypeCollection
        {
            get
            {
                return Global.actorTypeCollection;
            }
            set
            {
                Global.actorTypeCollection = value;
                RaisePropertyChanged("ActorTypeCollection");
            }
        }
        public ActorTypeViewModel SelectData
        {
            get
            {
                return _selectData;
            }
            set
            {
                _selectData = value;
                RaisePropertyChanged("SelectData");
            }
        }
        public Setting Setting
        {
            get
            {
                return Global.setting;
            }
            private set
            { }
        }

        public DelegateCommand WriteDefinitionCommand { get; private set; }
        public DelegateCommand AddDefinitionCommand { get; private set; }

        public ActorTypeView()
        {
            WriteDefinitionCommand = new DelegateCommand(WriteDefinition);
            AddDefinitionCommand = new DelegateCommand(AddActorType);
        }

        /*
         *  アクタータイプ定義を書き出す
         */
        public void WriteDefinition()
        {
            Global.Log.ShowWindow();
            Task.Run(() =>
            {
                foreach (var definition in ActorTypeCollection)
                {
                    string write_dir = Setting.DefinitionPath + "/ActorType/";
                    Global.Log.WriteLine("---- Write Actor Type -----------------");
                    using (StreamWriter writer = new StreamWriter(write_dir + definition.Name + ".yml", false, Encoding.UTF8))
                    {
                        StringBuilder builder = new StringBuilder();
                        ActorTypeViewModel.WriteYaml(ref builder, definition, "");
                        writer.Write(builder.ToString());
                        Global.Log.WriteLine($"write {definition.Name}");
                    }

                    try
                    {
                        if (!Directory.Exists(Setting.ActorTypeCodeGenDir))
                        {
                            Directory.CreateDirectory(Setting.ActorTypeCodeGenDir);
                        }
                    }
                    catch (Exception e)
                    {
                        Global.Log.WriteErrorLine(e);
                    }
                    // アクタータイプ毎の.cpp自動生成
                    try
                    {
                        var code_gen_cpp_path = $"{Setting.ActorTypeCodeGenDir}/{definition.Name}.cpp";
                        if (!File.Exists(code_gen_cpp_path))
                        {
                            using (StreamWriter cpp_writer = new StreamWriter(code_gen_cpp_path, false, Encoding.UTF8))
                            {
                                StringBuilder builder = new StringBuilder();
                                ActorTypeView.GenActorTypeCpp(ref builder, definition.Name);
                                cpp_writer.Write(builder.ToString());
                                Global.Log.WriteLine($"code gen: {code_gen_cpp_path}");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Global.Log.WriteErrorLine(e);
                    }
                    // アクタータイプ毎の.h自動生成
                    try
                    {
                        var code_gen_h_path = $"{Setting.ActorTypeCodeGenDir}/{definition.Name}.h";
                        if (!File.Exists(code_gen_h_path))
                        {
                            using (StreamWriter header_writer = new StreamWriter(code_gen_h_path, false, Encoding.UTF8))
                            {
                                StringBuilder builder = new StringBuilder();
                                ActorTypeView.GenActorTypeHeader(ref builder, definition.Name);
                                header_writer.Write(builder.ToString());
                                Global.Log.WriteLine($"code gen: {code_gen_h_path}");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Global.Log.WriteErrorLine(e);
                    }
                    // アクターファクトリーの.cpp自動生成
                    try
                    {
                        var code_gen_cpp_path = $"{Setting.ActorTypeCodeGenDir}/GameActorFactory.cpp";
                        using (StreamWriter cpp_writer = new StreamWriter(code_gen_cpp_path, false, Encoding.UTF8))
                        {
                            StringBuilder builder = new StringBuilder();
                            ActorTypeView.GenGameActorFactoryCpp(ref builder);
                            cpp_writer.Write(builder.ToString());
                            Global.Log.WriteLine($"code gen: {code_gen_cpp_path}");
                        }
                    }
                    catch (Exception e)
                    {
                        Global.Log.WriteErrorLine(e);
                    }
                }
                Global.Log.WriteLine("Finish Write Physics Info!");
                Global.Log.CloseWindow();
            });
        }

        /*
         *  アクタータイプ定義を読み込む
         */
        public void LoadActorType()
        {
            try
            {
                var dir_path = Setting.DefinitionPath + "/ActorType";
                var files = Directory.GetFiles(dir_path);
                foreach (var file in files)
                {
                    var add_definition = new ActorTypeViewModel();
                    add_definition.LoadYaml(file);
                    ActorTypeCollection.Add(add_definition);
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }

        /*
         *  アクタータイプ定義を追加する
         */
        public void AddActorType()
        {
            try
            {
                var add_definition = new ActorTypeViewModel();
                ActorTypeCollection.Add(add_definition);
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }
        /*
         *  自動生成コード
         */
        public static void GenActorTypeHeader(ref StringBuilder builder, string type_name)
        {
            builder.AppendLine("#pragma once");
            builder.AppendLine("");
            builder.AppendLine("#include <Aspergillus/Actor/ActorBase.h>");
            builder.AppendLine("");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.Append($"class {type_name} : public asp::ActorBase ");
            builder.AppendLine("{");
            builder.AppendLine("public:");
            builder.AppendLine($"    {type_name}();");
            builder.AppendLine("");
            builder.AppendLine("    virtual void prepare(const asp::ActorInstance::CreateArg& arg, const asp::ActorBase::DefaultParam& param) override;");
            builder.AppendLine("    virtual void update() override;");
            builder.AppendLine("");
            builder.AppendLine("private:");
            builder.AppendLine("};");
            builder.AppendLine("//---------------------------------------------------------------------");
        }
        public static void GenActorTypeCpp(ref StringBuilder builder, string type_name)
        {
            builder.AppendLine("");
            builder.AppendLine($"#include \"{type_name}.h\"");
            builder.AppendLine("");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.AppendLine($"{type_name}::{type_name}()");
            builder.Append("    : asp::ActorBase() ");
            builder.AppendLine("{");
            builder.AppendLine("}");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.Append($"void {type_name}::prepare(const asp::ActorInstance::CreateArg& arg, const asp::ActorBase::DefaultParam& param) ");
            builder.AppendLine("{");
            builder.AppendLine("    asp::ActorBase::prepare(arg, param);");
            builder.AppendLine("}");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.Append($"void {type_name}::update() ");
            builder.AppendLine("{");
            builder.AppendLine("}");
            builder.AppendLine("//---------------------------------------------------------------------");
        }
        public static void GenGameActorFactoryCpp(ref StringBuilder builder)
        {
            builder.AppendLine("");
            builder.AppendLine($"#include \"GameActorFactory.h\"");
            builder.AppendLine("");
            builder.AppendLine("// Actors");
            foreach (var type in Global.actorTypeCollection)
            {
                builder.AppendLine($"#include \"{type.Name}.h\"");
            }
            builder.AppendLine("");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.AppendLine("GameActorFactory::GameActorFactory()");
            builder.AppendLine("    : ActorFactoryBase() {");
            builder.AppendLine("}");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.AppendLine("GameActorFactory::~GameActorFactory() {");
            builder.AppendLine("}");
            builder.AppendLine("//---------------------------------------------------------------------");
            builder.AppendLine("std::unique_ptr<asp::ActorBase> GameActorFactory::CreateActor(const std::string& key) {");
            foreach (var type in Global.actorTypeCollection)
            {
                builder.Append($"    if (key == \"{type.Name}\") ");
                builder.AppendLine("{");
                builder.AppendLine($"        return std::make_unique<{type.Name}>();");
                builder.AppendLine("    }");
            }
            builder.AppendLine("    return std::make_unique<asp::ActorBase>();");
            builder.AppendLine("}");
            builder.AppendLine("//---------------------------------------------------------------------");
        }
    }
}
