﻿using ActorEditor.Common;
using ActorEditor.Model;
using ActorEditor.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.ViewModel
{
    class MainViewModel : WPFUtil.ViewModelBase
    {
        public ActorView ActorView { get; set; } = new ActorView();
        public PhysicsView PhysicsView { get; set; } = new PhysicsView();
        public ActorTypeView ActorTypeView { get; set; } = new ActorTypeView();
        public ModelView ModelView { get; set; } = new ModelView();

        public DelegateCommand OpenSettingWindow { get; private set; }
        
        public Setting Setting
        {
            get
            {
                return Global.setting;
            }
            set
            {
                Global.setting = value;
                RaisePropertyChanged("Setting");
            }
        }

        public MainViewModel()
        {
            OpenSettingWindow = new WPFUtil.DelegateCommand(openSettingWindow);

            LoadSetting();
            ActorTypeView.LoadActorType();
            PhysicsView.LoadPhysicsInfo();
            ActorView.LoadActorDefinition();
            ModelView.GatherModelList();
            ModelView.LoadModelDefinition();
        }

        public void LoadSetting()
        {
            Setting = new Setting();
            Setting.loadSetting();
        }
        
        
        public void openSettingWindow()
        {
            Window window = new SettingWindow();
            window.DataContext = Setting;
            window.Show();
        }
    }
}
