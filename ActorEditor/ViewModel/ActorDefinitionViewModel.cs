﻿using ActorEditor.Common;
using ActorEditor.Model;
using ActorEditor.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.ViewModel
{
    class ActorDefinitionViewModel : ViewModelBase
    {
        ActorDefinition definition = new ActorDefinition();

        public string Name
        {
            get
            {
                return definition._name;
            }
            set
            {
                definition._name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Type
        {
            get
            {
                return definition._type;
            }
            set
            {
                definition._type = value;
                RaisePropertyChanged("Type");
            }
        }
        public ObservableCollection<ActorTypeViewModel> ActorTypeCollection
        {
            get
            {
                return Global.actorTypeCollection;
            }
            private set {}
        }

        public string ModelKey
        {
            get
            {
                return definition._modelKey;
            }
            set
            {
                definition._modelKey = value;
                RaisePropertyChanged("ModelKey");
            }
        }
        public string PhysicsType
        {
            get
            {
                return definition._physicsType;
            }
            set
            {
                definition._physicsType = value;
                RaisePropertyChanged("PhysicsType");
            }
        }
        public ObservableCollection<PhysicsInfoViewModel> PhysicsInfoCollection
        {
            get
            {
                return Global.physicsInfoCollection;
            }
            private set {}
        }
        public ObservableCollection<ModelDefinitionViewModel> ModelDefinitionCollection
        {
            get
            {
                return Global.modelDefinitionCollection;
            }
            private set { }
        }

        public static void Write(ref StringBuilder builder, ActorDefinitionViewModel actor, string offset)
        {
            builder.AppendLine($"{offset}Type: {actor.Type}");
            builder.AppendLine($"{offset}Model: {actor.ModelKey}");
            builder.AppendLine($"{offset}PhysicsType: {actor.PhysicsType}");
        }
        public bool Load(string load_path)
        {
            var input = new StreamReader(load_path, Encoding.UTF8);
            var yaml = new YamlStream();
            yaml.Load(input);
            var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;

            this.Name = Path.GetFileNameWithoutExtension(load_path);
            if (YamlUtil.Contains(mapping, "Type"))
            {
                this.Type = mapping.Children["Type"].ToString();
            }
            else
            {
                WarningBox.showMessage($"{load_path}の読み込みに失敗");
                return false;
            }
            if (YamlUtil.Contains(mapping, "Model"))
            {
                this.ModelKey = mapping.Children["Model"].ToString();
            }
            else
            {
                WarningBox.showMessage($"{load_path}の読み込みに失敗");
                return false;
            }
            if (YamlUtil.Contains(mapping, "PhysicsType"))
            {
                this.PhysicsType = mapping.Children["PhysicsType"].ToString();
            }
            else
            {
                WarningBox.showMessage($"{load_path}の読み込みに失敗");
                return false;
            }
            return true;
        }
    }
}
