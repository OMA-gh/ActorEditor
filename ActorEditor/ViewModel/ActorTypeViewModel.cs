﻿using ActorEditor.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.ViewModel
{
    class ActorTypeViewModel: ViewModelBase
    {
        private ActorType actorType = new ActorType();

        public string Name
        {
            get
            {
                return actorType._name;
            }
            set
            {
                actorType._name = value;
                RaisePropertyChanged("Name");
            }
        }

        public static void WriteYaml(ref StringBuilder builder, ActorTypeViewModel definition, string offset)
        {
            builder.AppendLine($"{offset}Name: {definition.Name}");
        }
        public void LoadYaml(string path)
        {
            try
            {
                using (var input = new StreamReader(path, Encoding.UTF8))
                {
                    var yaml = new YamlStream();
                    yaml.Load(input);
                    Name = Path.GetFileNameWithoutExtension(path);
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }
    }
}
