﻿using ActorEditor.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.ViewModel
{
    class PhysicsInfoViewModel : ViewModelBase
    {
        PhysicsInfo _info = new PhysicsInfo();

        public string Name
        {
            get
            {
                return _info._name;
            }
            set
            {
                _info._name = value;
                RaisePropertyChanged("Name");
            }
        }
        public PhysicsType Type
        {
            get
            {
                return _info._type;
            }
            set
            {
                _info._type = value;
                RaisePropertyChanged("Type");
            }
        }
        public PhysicsType[] Types
        {
            get
            {
                return (PhysicsType[])Enum.GetValues(typeof(PhysicsType));
            }
            private set { }
        }
        public PhysicsShape Shape
        {
            get
            {
                return _info._shape;
            }
            set
            {
                _info._shape = value;
                RaisePropertyChanged("Shape");
            }
        }
        public PhysicsShape[] Shapes
        {
            get
            {
                return (PhysicsShape[])Enum.GetValues(typeof(PhysicsShape));
            }
            private set { }
        }
        public float CenterPosX
        {
            get
            {
                return _info._centerPosX;
            }
            set
            {
                _info._centerPosX = value;
                RaisePropertyChanged("CenterPosX");
            }
        }
        public float CenterPosY
        {
            get
            {
                return _info._centerPosY;
            }
            set
            {
                _info._centerPosY = value;
                RaisePropertyChanged("CenterPosY");
            }
        }
        public float CenterPosZ
        {
            get
            {
                return _info._centerPosZ;
            }
            set
            {
                _info._centerPosZ = value;
                RaisePropertyChanged("CenterPosZ");
            }
        }
        public string CenterPosStr
        {
            get
            {
                return $"{CenterPosX}, {CenterPosY} , {CenterPosZ}";
            }
            private set { }
        }
        public float ScaleX
        {
            get
            {
                return _info._scaleX;
            }
            set
            {
                _info._scaleX = value;
                RaisePropertyChanged("ScaleX");
            }
        }
        public float ScaleY
        {
            get
            {
                return _info._scaleY;
            }
            set
            {
                _info._scaleY = value;
                RaisePropertyChanged("ScaleY");
            }
        }
        public float ScaleZ
        {
            get
            {
                return _info._scaleZ;
            }
            set
            {
                _info._scaleZ = value;
                RaisePropertyChanged("ScaleZ");
            }
        }
        public string ScaleStr
        {
            get
            {
                return $"{ScaleX}, {ScaleY} , {ScaleZ}";
            }
            private set { }
        }
        public static void WriteYaml(ref StringBuilder builder, PhysicsInfoViewModel info, string offset)
        {
            builder.AppendLine($"{offset}Type: {info.Type.ToString()}");
            builder.AppendLine($"{offset}Shape: {info.Shape.ToString()}");
            builder.AppendLine($"{offset}CenterPos:");
            builder.AppendLine($"{offset}  X: {string.Format("{0:0.000000}", info.CenterPosX)}");
            builder.AppendLine($"{offset}  Y: {string.Format("{0:0.000000}", info.CenterPosY)}");
            builder.AppendLine($"{offset}  Z: {string.Format("{0:0.000000}", info.CenterPosZ)}");
            builder.AppendLine($"{offset}Scale:");
            builder.AppendLine($"{offset}  X: {string.Format("{0:0.000000}", info.ScaleX)}");
            builder.AppendLine($"{offset}  Y: {string.Format("{0:0.000000}", info.ScaleY)}");
            builder.AppendLine($"{offset}  Z: {string.Format("{0:0.000000}", info.ScaleZ)}");
        }
        public void LoadYaml(string path)
        {
            try
            {
                using (var input = new StreamReader(path, Encoding.UTF8))
                {
                    var yaml = new YamlStream();
                    yaml.Load(input);
                    Name = Path.GetFileNameWithoutExtension(path);

                    var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;
                    string type = mapping["Type"].ToString();
                    if (type.Equals("Dynamic"))
                    {
                        Type = PhysicsType.Dynamic;
                    }
                    else if (type.Equals("Static"))
                    {
                        Type = PhysicsType.Static;
                    }
                    else if (type.Equals("CharacterController"))
                    {
                        Type = PhysicsType.CharacterController;
                    }
                    string shape = mapping["Shape"].ToString();
                    if (shape.Equals("Box"))
                    {
                        Shape = PhysicsShape.Box;
                    }
                    else if (shape.Equals("Shape"))
                    {
                        Shape = PhysicsShape.Sphere;
                    }
                    else if (shape.Equals("Capsule"))
                    {
                        Shape = PhysicsShape.Capsule;
                    }
                    var center_pos = mapping["CenterPos"] as YamlMappingNode;
                    CenterPosX = float.Parse(center_pos["X"].ToString());
                    CenterPosY = float.Parse(center_pos["Y"].ToString());
                    CenterPosZ = float.Parse(center_pos["Z"].ToString());

                    var scale = mapping["Scale"] as YamlMappingNode;
                    ScaleX = float.Parse(scale["X"].ToString());
                    ScaleY = float.Parse(scale["Y"].ToString());
                    ScaleZ = float.Parse(scale["Z"].ToString());
                }
            }
            catch(Exception e)
            {
                WarningBox.showWarning(e);
            }
        }
    }
}
