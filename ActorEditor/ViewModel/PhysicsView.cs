﻿using ActorEditor.Common;
using ActorEditor.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.ViewModel
{
    class PhysicsView : ViewModelBase
    {
        private PhysicsInfoViewModel _selectData = null;

        public PhysicsInfoViewModel SelectData
        {
            get
            {
                return _selectData;
            }
            set
            {
                _selectData = value;
                RaisePropertyChanged("SelectData");
            }
        }
        public Setting Setting
        {
            get
            {
                return Global.setting;
            }
            set
            {
                Global.setting = value;
                RaisePropertyChanged("Setting");
            }
        }
        public ObservableCollection<PhysicsInfoViewModel> PhysicsInfoCollection
        {
            get
            {
                return Global.physicsInfoCollection;
            }
            set
            {
                Global.physicsInfoCollection = value;
                RaisePropertyChanged("PhysicsInfoCollection");
            }
        }

        public DelegateCommand WriteDefinitionCommand { get; private set; }
        public DelegateCommand AddDefinitionCommand { get; private set; }

        public PhysicsView()
        {
            WriteDefinitionCommand = new DelegateCommand(WritePhysicsInfo);
            AddDefinitionCommand = new DelegateCommand(AddPhysicsInfo);
        }

        /*
         *  物理定義を書き出す
         */
        public void WritePhysicsInfo()
        {
            Global.Log.ShowWindow();
            foreach (var info in PhysicsInfoCollection)
            {
                string write_dir = Setting.DefinitionPath + "/Physics/";
                Global.Log.WriteLine("---- Write Physics Info -----------------");
                using (StreamWriter writer = new StreamWriter(write_dir + info.Name + ".yml", false, Encoding.UTF8))
                {
                    StringBuilder builder = new StringBuilder();
                    PhysicsInfoViewModel.WriteYaml(ref builder, info, "");
                    writer.Write(builder.ToString());
                    Global.Log.WriteLine($"write {info.Name}");
                }
            }
            Global.Log.WriteLine("Finish Write Physics Info!");
            Global.Log.WriteLine("Write Physics List");
            string content_dir = Setting.ContentPath + "/Physics/";
            if (!Directory.Exists(content_dir))
            {
                Directory.CreateDirectory(content_dir);
            }
            Global.Log.WriteLine($"out : {content_dir + "PhysicsList.yml"}");
            using (StreamWriter writer = new StreamWriter(content_dir + "PhysicsList.yml", false, Encoding.UTF8))
            {
                StringBuilder builder = new StringBuilder();
                foreach (var info in PhysicsInfoCollection)
                {
                    builder.AppendLine($"{info.Name}:");
                    PhysicsInfoViewModel.WriteYaml(ref builder, info, "  ");
                }
                writer.Write(builder.ToString());
            }
            Global.Log.WriteLine("Finish Write ActorList!");
            Global.Log.CloseWindow();
        }

        /*
         *  物理定義を読み込む
         */
        public void LoadPhysicsInfo()
        {
            try
            {
                var dir_path = Setting.DefinitionPath + "/Physics";
                var files = Directory.GetFiles(dir_path);
                foreach (var file in files)
                {
                    PhysicsInfoViewModel physics_info = new PhysicsInfoViewModel();
                    physics_info.LoadYaml(file);
                    PhysicsInfoCollection.Add(physics_info);
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }

        /*
         *  アクター定義を追加する
         */
        public void AddPhysicsInfo()
        {
            try
            {
                var physics_info = new PhysicsInfoViewModel();
                PhysicsInfoCollection.Add(physics_info);
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
        }
    }
}
