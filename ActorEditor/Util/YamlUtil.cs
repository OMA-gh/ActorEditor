﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFUtil;
using YamlDotNet.RepresentationModel;

namespace ActorEditor.Util
{
    class YamlUtil
    {
        public static bool Contains(YamlMappingNode yaml, string key)
        {
            try
            {
                if (yaml.Children.Keys.Contains(key))
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                WarningBox.showWarning(e);
            }
            return false;
        }
    }
}
