﻿#pragma once

#include <Aspergillus/Actor/ActorBase.h>

//---------------------------------------------------------------------
class Simple : public ActorBase {
public:
    Simple();

    virtual void prepare(const ActorInstance::CreateArg& arg, const ActorBase::DefaultParam& param) override;
    virtual void update() override;

private:
};
//---------------------------------------------------------------------
