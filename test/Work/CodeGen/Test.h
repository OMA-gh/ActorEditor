﻿#pragma once

#include <Aspergillus/Actor/ActorBase.h>

//---------------------------------------------------------------------
class Test : public ActorBase {
public:
    Test();

    virtual void prepare(const ActorInstance::CreateArg& arg, const ActorBase::DefaultParam& param) override;
    virtual void update() override;

private:
};
//---------------------------------------------------------------------
