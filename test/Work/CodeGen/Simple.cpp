﻿
#include "Simple.h"

//---------------------------------------------------------------------
Simple::Simple() : ActorBase() {
}
//---------------------------------------------------------------------
void Simple::prepare(const ActorInstance::CreateArg& arg, const ActorBase::DefaultParam& param) {
    ActorBase::prepare(arg, param);
}
//---------------------------------------------------------------------
void Simple::update() {
}
//---------------------------------------------------------------------
