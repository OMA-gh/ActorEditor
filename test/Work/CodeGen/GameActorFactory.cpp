﻿
#include "GameActorFactory.h"

// Actors
#include "Simple.h"
#include "Test.h"

//---------------------------------------------------------------------
GameActorFactory::GameActorFactory()
    : ActorFactoryBase() {
}
//---------------------------------------------------------------------
GameActorFactory::~GameActorFactory() {
}
//---------------------------------------------------------------------
std::unique_ptr<asp::ActorBase> GameActorFactory::CreateActor(const std::string& key) {
    if (key == "Simple") {
        return std::make_unique<Simple>();
    }
    if (key == "Test") {
        return std::make_unique<Test>();
    }
    return std::make_unique<asp::ActorBase>();
}
//---------------------------------------------------------------------
